> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930

## Kristopher Mangahas

### Project 2 Requirements:


* Include splash screen (optional 10 additional pts.).
* Insert at least five sample tasks (see p. 413, and screenshot below).
* Test database class (see pp. 424, 425)
* Must add background color(s) or theme
* Create and display launcher icon image (optional)

#### Assignment Screenshots:


*Screenshot of running App's Splash Screen*:

![Unpopulated Interface](img/splash.png)


*Sreenshot of running App's Main Page*:

![Populated Interface](img/main.png)


