> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930

## Kristopher Mangahas

### Assignment 3 Requirements:


* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s unpopulated user interface;
* Screenshot of running application’s toast notification interface;
* Screenshot of running application's coverted currency user interface;



#### Assignment Screenshots:


*Screenshot of running App's Unpopulated Interface*:

![Unpopulated Interface](img/start.png)


*Sreenshot of running App's toast notification*:

![Populated Interface](img/toast.png)


*Sreenshot of running App's Populated Interface*:

![Populated Interface](img/done.png)
