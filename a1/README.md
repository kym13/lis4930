> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930

## Kristopher Mangahas

### Assignment 1 Requirements:


* Screenshot of JDK java Hello
* Screenshot of Running Android Studio My First App
* Screenshot of Running Android Studio Contacts APP
* git commands w/short Descriptions
* Bitbucket repo links.



> #### Git commands w/short descriptions:

1. git init: Create an empty Git repository or reinitialize an existing one
2. git status: Show the working tree status
3. git add: Add file contents to the index
4. git commit: Record changes to the repository
5. git push: Update remote refs along with associated objects
6. git pull: Fetch from and integrate with another repository or a local branch
7. git checkout: Switch branches or restore working tree files

#### Assignment Screenshots:


*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/hello.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)

*Screenshot of Android Studio - Contacts*:

![Android Studio Installation Screenshot](img/contacts.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kym13/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kym13/myteamquotes/ "My Team Quotes Tutorial")
