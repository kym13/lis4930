> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930

## Kristopher Mangahas

### Assignment 3 Requirements:


* Include splash screen image, app title, intro text.
* Include appropriate images.
* Must use persistent data: SharedPreferences
* Widgets and images must be vertically and horizontally aligned.
* Must add background color(s) or theme
* Create and display launcher icon image


#### Assignment Screenshots:

*Screenshot of running App's Splash Screen*:

![Unpopulated Interface](img/splash.png)

*Screenshot of running App's Unpopulated Interface*:

![Unpopulated Interface](img/main.png)


*Sreenshot of Incorrect Input*:

![Populated Interface](img/fail.png)


*Sreenshot of Correct Input*:

![Populated Interface](img/pass.png)
