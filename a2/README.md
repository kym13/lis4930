> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930

## Kristopher Mangahas

### Assignment 2 Requirements:


* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s unpopulated user interface;
* Screenshot of running application’s populated user interface;
* Provide Bitbucket read-only access to lis4930 repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax (README.md must also include screenshots as per above.)
* Blackboard Links: lis4930 Bitbucket repo



#### Assignment Screenshots:


*Screenshot of running App's Unpopulated Interface*:

![Unpopulated Interface](img/start.png)

*Sreenshot of running App's Populated Interface*:

![Populated Interface](img/done.png)
