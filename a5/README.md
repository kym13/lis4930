> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930

## Kristopher Mangahas

### Assignment 5 Requirements:


* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s splash screen (list of articles – activity_items.xml);
* Screenshot of running application’s individual article (activity_item.xml);
* Screenshots of running application’s default browser (article link);


#### Assignment Screenshots:

*Screenshot of running App's Splash Screen*:

![Unpopulated Interface](img/splash.png)

*Screenshot of an Individual Article*:

![Unpopulated Interface](img/item.png)


*Sreenshot of default browser*:

![Populated Interface](img/browse.png)


*Sreenshot of chrome*:

![Populated Interface](img/internet.png)
