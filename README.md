> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930 Special Topics

## Kristopher Mangahas

### LIS4930 Requirements:

*Course Work Links:*

1. [A1](a1/README.md)

* Screenshot of JDK java Hello
* Screenshot of Running Android Studio My First App
* Screenshot of Running Android Studio Contacts APP
* git commands w/short Descriptions
* Bitbucket repo links.


2.[A2](a2/README.md)

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s unpopulated user interface;
* Screenshot of running application’s populated user interface;
* Provide Bitbucket read-only access to lis4930 repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax (README.md must also include screenshots as per above.)
* Blackboard Links: lis4930 Bitbucket repo


3.[A3](a3/README.md)

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s unpopulated user interface;
* Screenshot of running application’s toast notification interface;
* Screenshot of running application's coverted currency user interface.


4.[P1](p1/README.md)

* Include splash screen image, app title, intro text.
* Include artists’ images and media.
* Images and buttons must be vertically and horizontally aligned.
* Must add background color(s) or theme
* Create and display launcher icon image
* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s splash screen;
* Screenshot of running application’s follow-up screen (with images and buttons);
* Screenshots of running application’s play and pause user interfaces (with images and buttons);


5.[A4](a4/README.md)

* Include splash screen image, app title, intro text.
* Include appropriate images.
* Must use persistent data: SharedPreferences
* Widgets and images must be vertically and horizontally aligned.
* Must add background color(s) or theme
* Create and display launcher icon image

6.[A5](a5/README.md)

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s splash screen (list of articles – activity_items.xml);
* Screenshot of running application’s individual article (activity_item.xml);
* Screenshots of running application’s default browser (article link);

6.[P2](p2/README.md)

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s Task List (optional splash screen: 10 additional pts.);


